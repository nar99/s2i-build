# S2i Build

OCP4.6 S2I빌드 관련 샘플 리소스
# 

## https://docs.openshift.com/container-platform/4.6/builds/understanding-image-builds.html#builds-strategy-s2i-build_understanding-image-builds
## https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/7.3/html-single/getting_started_with_jboss_eap_for_openshift_container_platform/index#eap_s2i_process

### S2I Build > Maven Single Project
```
$ oc process -f https://gitlab.com/injeinc-cnp/s2i-build/-/raw/master/eap73-openjdk11-basic-s2i-default.yaml APPLICATION_NAME=eap-app03 SOURCE_REPOSITORY_URL=https://gitlab.com/injeinc-cnp/s2i-build.git CONTEXT_DIR=sht -o yaml | oc create -f -
```

### S2I Build > Maven Multi Module Project
```
$ oc process -f https://gitlab.com/injeinc-cnp/s2i-build/-/raw/master/eap73-openjdk11-basic-s2i-default.yaml APPLICATION_NAME=eap-app04 SOURCE_REPOSITORY_URL=https://gitlab.com/injeinc-cnp/s2i-build.git CONTEXT_DIR=sht-module ARTIFACT_DIR=sht-webapp/target -o yaml | oc create -f -

$ oc process -f https://gitlab.com/injeinc-cnp/s2i-build/-/raw/master/eap73-openjdk11-basic-s2i-default.yaml APPLICATION_NAME=eap-app05 SOURCE_REPOSITORY_URL=https://gitlab.com/injeinc-cnp/s2i-build.git CONTEXT_DIR=sht-module ARTIFACT_DIR=sht-webapp/target,sht-webapp-a/target -o yaml | oc create -f -
```
